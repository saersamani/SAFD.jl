using SAFD
using Test
using Pkg
#using MS_Import

#"""

try 
    using MS_Import
    #using Cent2Profile
catch
    @warn("MS_Import is being installed")
    Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/ms_import.jl/src/master/"))
    #Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/cent2profile.jl/src/master/"))
    using MS_Import
    #using Cent2Profile
end


@testset "Dependency Check" begin


    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    chrom=import_files(path,filenames,mz_thresh)
    @test size(chrom["MS1"]["Rt"],1) == 43
end

@testset "safd" begin


    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    chrom=import_files(path,filenames,mz_thresh)
    m=split("test_chrom.jld",".")



    max_numb_iter=10
    max_t_peak_w=20
    res=20000
    min_ms_w=0.02
    r_thresh=0.85
    min_int=2000
    sig_inc_thresh=5
    S2N=2

    min_peak_w_s=3



    rep_table,final_table=safd(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity"],chrom["MS1"]["Rt"],
        m[1],pwd(),max_numb_iter,max_t_peak_w,res,min_ms_w,r_thresh,
        min_int,sig_inc_thresh,S2N,min_peak_w_s)

    #println(final_table)

    @test final_table[!,"Int"][1] == 230546.0
end


@testset "safd_s3D" begin


    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    chrom=import_files(path,filenames,mz_thresh)
    m=split("test_chrom.jld",".")

    max_numb_iter=10
    max_t_peak_w=300
    res=20000
    min_ms_w=0.02
    r_thresh=0.85
    min_int=2000
    sig_inc_thresh=5
    S2N=2

    min_peak_w_s=3



    rep_table,final_table=safd_s3D(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity"],chrom["MS1"]["Rt"],
    m[1],pwd(),max_numb_iter,max_t_peak_w,res,min_ms_w,r_thresh,
        min_int,sig_inc_thresh,S2N,min_peak_w_s)

    #println(final_table)

    @test final_table[!,"Int"][1] == 230546.0
end



@testset "safd_s3d_cent" begin

    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    max_numb_iter=10
    max_t_peak_w=300
    res=20000
    min_ms_w=0.02
    r_thresh=0.85
    min_int=5000
    sig_inc_thresh=5
    S2N=2

    min_peak_w_s=3

    chrom=import_files(path,filenames,mz_thresh)
    m=split("test_chrom.jld",".")




    mz_val_cent,mz_int_cent,dm_c = centroid(chrom["MS1"]["Mz_values"],chrom["MS1"]["Mz_intensity"],min_int,res); 

    mz_vals = deepcopy(mz_val_cent)
    mz_int = deepcopy(mz_int_cent)

    mdm = deepcopy(dm_c);


    method =  "BG"

    rep_table1,final_table1 = safd_s3d_cent(mz_vals,mz_int,chrom["MS1"]["Rt"],m[1],pwd(),max_numb_iter,
        max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,method)

    #println(rep_table1)

    method = "RFM" 
    mz_vals = deepcopy(mz_val_cent)
    mz_int = deepcopy(mz_int_cent)

    rep_table2,final_table2 = safd_s3d_cent(mz_vals,mz_int,chrom["MS1"]["Rt"],m[1],pwd(),max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,method)

    method = "MDM" 

    mz_vals = deepcopy(mz_val_cent)
    mz_int = deepcopy(mz_int_cent)

    rep_table3,final_table3 = safd_s3d_cent(mz_vals,mz_int,chrom["MS1"]["Rt"],m[1],pwd(),max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_thresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,method,mdm)


    @test final_table1[!,"MeasMass"][1] == 199.1628
    @test final_table2[!,"MeasMass"][1] == 199.1628
    @test final_table3[!,"MeasMass"][1] == 199.1628
    
end

#"""

@testset "Alignment" begin

    
    path2files = pwd() 

    v_n_mz = "MeasMass"
    v_n_rt = "Rt"
    v_n_int = "Int"
    mz_tol = 0.02
    rt_tol = 0.1
    
    # Internal
    int_table, a_table, r_table = feature_align(path2files)

    #println(int_table)
    
    # Internal with tols 
    int_table, a_table, r_table = feature_align(path2files,mz_tol,rt_tol)
    
    # External 
    
    int_table = feature_align(path2files,mz_tol,rt_tol,v_n_mz,v_n_rt,v_n_int)

    @test length(int_table.Nr) == 7
    @test int_table[!,"test_chrom_Cent_report.csv"][1] == 41112
end


@testset "Targeted Feature detection" begin

    
    path=""
    filenames=["test_chrom.mzXML"]
    mz_thresh=[0,600]

    chrom=import_files(path,filenames,mz_thresh)

    mz_tol = 0.01
    min_int = 500
    mz = 199.1628
     
    table = targetfeature(chrom,mz,mz_tol,min_int)

    @test table.Rt[1] == 6.146

    
end
