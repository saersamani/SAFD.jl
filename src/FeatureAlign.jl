using CSV
using DataFrames
using Glob
using Statistics
using XLSX


###########################################
# A function to import the files


function report_import_internal(path2reports)

    nn = readdir(path2reports)
    Rep = DataFrame()
    names = []
    c = 1
    for i =1:size(nn,1)
        m = split(nn[i,:][1],".")
        if length(m[1]) == 0 || m[end] != "csv" || m[end-1][end-5:end] != "report"
            continue

        end 
        f_n = joinpath(path2reports,nn[i,:][1])
        df = DataFrame(CSV.File(f_n))
        df1 = hcat(DataFrame(F_ID= c .* Int.(ones(df.Nr[end]))),df)
        Rep = vcat(Rep,df1)
        names = vcat(names,nn[i])
        c = c+1

    end 
    
    return(Rep,names)


end

###########################################
# A function to import the files


function report_import_external(path2files,v_n_mz,v_n_rt,v_n_int)

    nn = readdir(path2files)
    Rep = DataFrame()
    names = []
    c = 1
    for i =1:size(nn,1)
        m = split(nn[i,:][1],".")
        if length(m[1]) == 0 || m[end] != "csv" || m[end-1][end-6:end] != "_report"
            continue

        end 
        f_n = joinpath(path2files,nn[i,:][1])
        df = DataFrame(CSV.File(f_n))
        df_ = DataFrame(Rt = df[!,v_n_rt],MeasMass = df[!,v_n_mz], Int = df[!,v_n_int])
        df1 = hcat(DataFrame(F_ID= c .* Int.(ones(size(df,1)))),df_)
        Rep = vcat(Rep,df1)
        names = vcat(names,nn[i])
        c = c+1

    end 

    #Rep_ = DataFrame(F_ID = Rep[!,"F_ID"],Rt = Rep[!,v_n_rt],MeasMass = Rep[!,v_n_mz], Int = Rep[!,v_n_int])
    
    return(Rep,names)


end

###########################################
# A function to select candidate featuters retention time 

function select_candidates_rt(rep,ind)

    pf = rep[ind,:]
    med_scan = median(rep.ScanInPeak)

    if pf.ScanNum - med_scan <= 0
        lb = 1
        ub = pf.ScanNum + med_scan
    elseif pf.ScanNum + med_scan >= maximum(rep.ScanNum)
        lb = pf.ScanNum - med_scan
        ub =  maximum(rep.ScanNum)
    elseif pf.ScanNum - med_scan > 0 && pf.ScanNum + med_scan < maximum(rep.ScanNum)
        lb = pf.ScanNum - med_scan
        ub = pf.ScanNum + med_scan
    end 
    
    sel_inds = findall(x -> ub >= x >= lb, rep.ScanNum)

    return sel_inds
end


###########################################
# A function to select candidate featuters retention time 

function select_candidates_rt_(rep,ind,rt_tol)

    pf = rep[ind,:]
    

    if pf.Rt - rt_tol <= 0
        lb = 0
        ub = pf.Rt + rt_tol
    elseif pf.Rt + rt_tol >= maximum(rep.Rt)
        lb = pf.Rt - rt_tol
        ub =  maximum(rep.Rt)
    elseif pf.Rt - rt_tol > 0 && pf.Rt + rt_tol < maximum(rep.Rt)
        lb = pf.Rt - rt_tol
        ub = pf.Rt + rt_tol
    end 
    
    sel_inds = findall(x -> ub >= x >= lb, rep.Rt)

    return sel_inds
end


#####################################################################################
# A function to select candidate featuters mass 

function select_candidates_mz(rep,ind,sel_inds)

    pf = rep[ind,:]
    
    s_f = rep[sel_inds,:]

    med_mz_tol = median(s_f.MaxMass .- s_f.MinMass)

    
    sel_inds_f = sel_inds[findall(x -> pf.MeasMass + med_mz_tol >= x >= pf.MeasMass - med_mz_tol, rep.MeasMass[sel_inds])]

    return sel_inds_f
end

#####################################################################################
# A function to select candidate featuters mass with a mass tolerances

function select_candidates_mz_(rep,ind,sel_inds,mz_tol)

    pf = rep[ind,:]

    
    sel_inds_f = sel_inds[findall(x -> pf.MeasMass + mz_tol >= x >= pf.MeasMass - mz_tol, rep.MeasMass[sel_inds])]

    return sel_inds_f
end

#####################################################################################
# A function to group select candidate featuters 

function group_candidates_internal(rep,sel_inds_f)

    #pf = rep[ind,:]
    #sel_inds_f1 = deepcopy(sel_inds_f)
    

    AvScan = round(mean(rep.ScanNum[sel_inds_f]))
    MaxScanInPeak = maximum(rep.ScanInPeak[sel_inds_f])
    MinRt = minimum(rep.RtStart[sel_inds_f])
    MaxRt = maximum(rep.RtEnd[sel_inds_f])
    AveRt = round(mean([MinRt,MaxRt]),digits =2)
    MinMass = minimum(rep.MinMass[sel_inds_f])
    MaxMass = maximum(rep.MaxMass[sel_inds_f])
    AveMass = round(mean(rep.MeasMass[sel_inds_f]),digits =4)

    


    ind_files = findall(x -> x ==1, rep.Nr)

    Int_ = zeros(1,length(ind_files))
    Area = zeros(1,length(ind_files))
    Res = zeros(1,length(ind_files));
    
   for i =1:length(ind_files)
        #println(i)

        sele_f = rep[sel_inds_f,:]

        selected_f = sele_f[sele_f.F_ID .== i,:]
        if size(selected_f,1) >1
            Int_[i] = maximum(selected_f.Int)
            Area[i] = maximum(selected_f.Area)
            temp_r = selected_f.MediRes[selected_f.MediRes .!= Inf]
            if isempty(temp_r)
                Res[i] = Inf
            else
                Res[i] = median(temp_r)
            end
        elseif size(selected_f,1) == 1
            Int_[i] = selected_f.Int[1]
            Area[i] = selected_f.Area[1]
            Res[i] = selected_f.MediRes[1]

        end 
        

   end 

    return(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area,Res)
end

#####################################################################################
# A function to group select candidate featuters 

function group_candidates_external(rep,sel_inds_f)

    
    MinRt = minimum(rep.Rt[sel_inds_f])
    MaxRt = maximum(rep.Rt[sel_inds_f])
    AveRt = round(mean([MinRt,MaxRt]),digits =2)
    MinMass = minimum(rep.MeasMass[sel_inds_f])
    MaxMass = maximum(rep.MeasMass[sel_inds_f])
    AveMass = round(mean(rep.MeasMass[sel_inds_f]),digits =4)

    


    ind_files = unique(rep.F_ID)

    Int_ = zeros(1,length(ind_files))
   
    
   for i =1:length(ind_files)
        #println(i)

        sele_f = rep[sel_inds_f,:]

        selected_f = sele_f[sele_f.F_ID .== i,:]
        if size(selected_f,1) >1
            Int_[i] = maximum(selected_f.Int)
           
        elseif size(selected_f,1) == 1
            Int_[i] = selected_f.Int[1]
            

        end 
        

   end 

    return(MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_)
end


#####################################################################################
# A function to align features generated via SAFD 

function feature_align_internal(path2files)

    rep,name = report_import_internal(path2files)

    if length(name) ==0
        @warn "No file has been imported."
        return res =[]
    end 

    feature = zeros(1,8)
    Inten = zeros(1,length(name))
    Area = zeros(1,length(name))
    Res = zeros(1,length(name))
    
    for i =1:size(rep,1)
        if rep.Rt[i] == 0
            continue
        end 
        ind = i
        println(i)
        sel_inds = select_candidates_rt(rep,ind)
        sel_inds_f = select_candidates_mz(rep,ind,sel_inds)
        AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area_,Res_ = group_candidates_internal(rep,sel_inds_f)
        rep.Rt[sel_inds_f] .= 0
        feature = vcat(feature,hcat(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass))
        Inten = vcat(Inten,Int_)
        Area = vcat(Area,Area_)
        Res = vcat(Res,Res_)

    end 

    table1=DataFrame(hcat(1:size( feature,1)-1,feature[2:end,:]),[:Nr,:AveScanNum,:MaxScanInPeak,:MinRt,:MaxRt,:AveRt,
    :MinMass,:MaxMass,:AveMass])
    

    table_int=DataFrame(Inten[2:end,:], :auto)
    table_are=DataFrame(Area[2:end,:], :auto)
    table_res=DataFrame(Res[2:end,:], :auto)

    rename!(table_int,Symbol.(name))
    rename!(table_are,Symbol.(name))
    rename!(table_res,Symbol.(name))

    table_int_f=hcat(table1,table_int)
    table_are_f=hcat(table1,table_are)
    table_res_f=hcat(table1,table_res)

    sort!(table_int_f,[:AveScanNum,:AveMass])
    sort!(table_are_f,[:AveScanNum,:AveMass])
    sort!(table_res_f,[:AveScanNum,:AveMass])

    table_int_f.Nr = 1:size( feature,1)-1
    table_are_f.Nr = 1:size( feature,1)-1
    table_res_f.Nr = 1:size( feature,1)-1

    output_int=joinpath(path2files,"FeatureList_Aligned_int.csv")
    output_area=joinpath(path2files,"FeatureList_Aligned_area.csv")

    CSV.write(output_int,table_int_f)
    CSV.write(output_area,table_are_f)

    #XLSX.writetable(output, Intensities=(collect(DataFrames.eachcol(table_int_f)),
    # DataFrames.names(table_int_f)  ), Areas=(collect(DataFrames.eachcol(table_are_f)),
    #  DataFrames.names(table_are_f)  ), Resolutions=(collect(DataFrames.eachcol(table_res_f)),
    #   DataFrames.names(table_res_f)  ), overwrite=true)

    return( table_int_f,table_are_f,table_res_f)
    


end


#####################################################################################
# A function to align features generated via SAFD 

function feature_align_internal_wp(path2files,mz_tol,rt_tol)

    rep,name = report_import_internal(path2files)

    if length(name) ==0
        @warn "No file has been imported."
        return res =[]
    end 

    feature = zeros(1,8)
    Int = zeros(1,length(name))
    Area = zeros(1,length(name))
    Res = zeros(1,length(name))
    
    for i =1:size(rep,1)
        if rep.Rt[i] == 0
            continue
        end 
        ind = i
        println(i)
        sel_inds = select_candidates_rt_(rep,ind,rt_tol)
        sel_inds_f = select_candidates_mz_(rep,ind,sel_inds,mz_tol)
        AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_,Area_,Res_ = group_candidates_internal(rep,sel_inds_f)
        rep.Rt[sel_inds_f] .= 0
        feature = vcat(feature,hcat(AvScan,MaxScanInPeak, MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass))
        Int = vcat(Int,Int_)
        Area = vcat(Area,Area_)
        Res = vcat(Res,Res_)

    end 

    table1=DataFrame(hcat(1:size( feature,1)-1,feature[2:end,:]),[:Nr,:AveScanNum,:MaxScanInPeak,:MinRt,:MaxRt,:AveRt,
    :MinMass,:MaxMass,:AveMass])
    

    table_int=DataFrame(Int[2:end,:], :auto)
    table_are=DataFrame(Area[2:end,:], :auto)
    table_res=DataFrame(Res[2:end,:], :auto)

    rename!(table_int,Symbol.(name))
    rename!(table_are,Symbol.(name))
    rename!(table_res,Symbol.(name))

    table_int_f=hcat(table1,table_int)
    table_are_f=hcat(table1,table_are)
    table_res_f=hcat(table1,table_res)

    sort!(table_int_f,[:AveScanNum,:AveMass])
    sort!(table_are_f,[:AveScanNum,:AveMass])
    sort!(table_res_f,[:AveScanNum,:AveMass])

    table_int_f.Nr = 1:size( feature,1)-1
    table_are_f.Nr = 1:size( feature,1)-1
    table_res_f.Nr = 1:size( feature,1)-1

    output_int=joinpath(path2files,"FeatureList_Aligned_int.csv")
    output_area=joinpath(path2files,"FeatureList_Aligned_area.csv")

    CSV.write(output_int,table_int_f)
    CSV.write(output_area,table_are_f)

    #XLSX.writetable(output, Intensities=(collect(DataFrames.eachcol(table_int_f)),
    # DataFrames.names(table_int_f)  ), Areas=(collect(DataFrames.eachcol(table_are_f)),
    #  DataFrames.names(table_are_f)  ), Resolutions=(collect(DataFrames.eachcol(table_res_f)),
    #   DataFrames.names(table_res_f)  ), overwrite=true)

    return( table_int_f,table_are_f,table_res_f)
    


end


#####################################################################################
# A function to align features generated by other feature detection algorithms

function feature_align_external(path2files,mz_tol,rt_tol,v_n_mz,v_n_rt,v_n_int)

    rep,name = report_import_external(path2files,v_n_mz,v_n_rt,v_n_int)

    if length(name) ==0
        @warn "No file has been imported."
        return res =[]
    end 

    feature = zeros(1,6)
    Inten = zeros(1,length(name))

    
    
    for i =1:size(rep,1)
        if rep.Rt[i] == 0
            continue
        end 
        ind = i
        println(i)
        sel_inds = select_candidates_rt_(rep,ind,rt_tol)
        sel_inds_f = select_candidates_mz_(rep,ind,sel_inds,mz_tol)
        MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass,Int_ = group_candidates_external(rep,sel_inds_f)
        rep.Rt[sel_inds_f] .= 0
        feature = vcat(feature,hcat(MinRt,MaxRt,AveRt,MinMass,MaxMass,AveMass))
        Inten = vcat(Inten,Int_)

    end 

    table1=DataFrame(hcat(1:size( feature,1)-1,feature[2:end,:]),[:Nr,:MinRt,:MaxRt,:AveRt,
    :MinMass,:MaxMass,:AveMass])
    

    table_int=DataFrame(Inten[2:end,:], :auto)
   

    rename!(table_int,Symbol.(name))
    

    table_int_f=hcat(table1,table_int)
   

    sort!(table_int_f,[:AveRt,:AveMass])
  

    table_int_f.Nr = 1:size( feature,1)-1
   

    output=joinpath(path2files,"FeatureList_Aligned_int.csv")
    CSV.write(output,table_int_f)

    #XLSX.writetable(output, Intensities=(collect(DataFrames.eachcol(table_int_f)),
    # DataFrames.names(table_int_f)  ), overwrite=true)

    return(table_int_f)
    


end


#####################################################################################
# A function to align features 

function feature_align(path2files,mz_tol::Float64=0.0,rt_tol::Float64=0.1,v_n_mz::String="",v_n_rt::String="",v_n_int::String="")

    if length(v_n_mz) > 0 && mz_tol > 0

        # External
        println("These reports will be aligned as externally generated ones.")
        feature_align_external(path2files,mz_tol,rt_tol,v_n_mz,v_n_rt,v_n_int)

    elseif length(v_n_mz) == 0 && mz_tol == 0
        # Internal 
        println("These reports will be aligned as internally generated ones (i.e. via SAFD).")
        feature_align_internal(path2files)

    elseif length(v_n_mz) == 0 && mz_tol > 0
        # Internal with Set tolerances
        println("These reports will be aligned as internally generated ones (i.e. via SAFD) with set mass and retention tolerance.")
        feature_align_internal_wp(path2files,mz_tol,rt_tol)
        
    end 


    
end






###########################################
# test area ""


"""
path2files = "/Volumes/SAER HD/Data/Temp_files/Phil/Test_align/" 
v_n_mz = "MeasMass"
v_n_rt = "Rt"
v_n_int = "Int"
mz_tol = 0.02
rt_tol = 0.1

# Internal
feature_align(path2files)

# Internal with tols 
feature_align(path2files,mz_tol,rt_tol)

# External 

feature_align(path2files,mz_tol,rt_tol,v_n_mz,v_n_rt,v_n_int)


""" 