
using DataFrames
using LsqFit
using Statistics
using BenchmarkTools
using FileIO
using PyCall 
using ScikitLearn
using Conda
using ProgressBars

#Conda.add("joblib")
#jl = pyimport("joblib")

#@sk_import linear_model: LogisticRegression
@sk_import ensemble: RandomForestRegressor



################################################################
# This module is to convert centroided HRMS data to profile data.
#
#
################################################################

################################################################
# Function for predicting the profile

function prof_pred(features,dm_per,int_c,f)

    @.model_1(x, p) = (1/(p[2]*sqrt(pi*2))) * exp(-(x-p[1])^2 / (2*p[2]^2))
    @.model_2(x, p) = (1/pi)*((p[2])/((x - p[1])^2 + (p[2])^2))


    p=zeros(2)
    mz_pred = 0;
    int_pred = 0;

    for j=1:length(dm_per)

        #p[1] = int_c[j]
        p[1] = features[j,2]
        p[2] = dm_per[j]/2000
        #ms_vec = features[j,2]-(2*p[2]):p[2]:features[j,2]+(2*(2*p[2]))
        ms_vec = range(features[j,2]-(2*p[2]), stop=features[j,2]+(2*(2*p[2])), length=8)
        if f == "gs"
            y_r=model_1(ms_vec,p)
        elseif f == "scd"
            y_r=model_2(ms_vec,p)
        else
            println("At the moment only Gassuian (gs) and Cauchy (scd) are enabled.
            Your data will be processed with the Gaussian model")
            y_r=model_1(ms_vec,p)
        end


        y_pred = int_c[j] * (y_r/maximum(y_r))
        y_pred =vcat(0,y_pred,0)
        ms_vec = vcat(ms_vec[1]-p[2],ms_vec,ms_vec[end]+p[2])
        # oplot( ms_vec,y_pred,"g:")
        # oplot( ms_vec,y_pred_sc,"b-.")

        mz_pred = hcat(mz_pred,ms_vec')
        int_pred = hcat(int_pred,y_pred')

    end

    return(mz_pred,int_pred)


end




################################################################
# Wrapping function

function cent2profile(mz_val_cent,mz_int_cent,fun="gs")

    mm=pathof(SAFD)
    path2aux=joinpath(mm[1:end-7],"QTOF_model.joblib")
    println("The model is being loaded. This may take a few minutes.")
    #println(path2aux)
    #model = jl.load("/Users/saersamanipour/Desktop/dev/pkgs/cent2profile.jl/src/QTOF_model.joblib")
    model = jl.load(path2aux)
    model.verbose = 0;

    mz_prof_pred = zeros(size(mz_val_cent,1),100*size(mz_val_cent,2));
    int_prof_pred = zeros(size(mz_val_cent,1),100*size(mz_val_cent,2));

    # i=100
    #@showprogress 1 "The data is being converted..." for i =1:size(mz_val_cent,1)
    #    sleep(0.5)
    for i in ProgressBar(1:size(mz_val_cent,1))
    #for i=1:size(mz_val_cent,1)
    #    println(i)
        ms = mz_val_cent[i,:];
        ms_int = mz_int_cent[i,:];
        if length(ms_int[ms_int .>0])>0
            rel_ms_int = 100 .* (ms_int ./ maximum(ms_int));
            rt_fact = 100 * i/size(mz_val_cent,1)
            features = hcat(rt_fact * ones(length(ms[ms .>0]),1),ms[ms .>0],rel_ms_int[ms .>0])
            dm_per = predict(model, features);

            # fun = "scd"
            # fun = "gs"
            mz_pred,int_pred = prof_pred(features,dm_per,ms_int[ms .>0],fun);

            # stem(ms,ms_int)
            # oplot(mz_val[i,:],mz_int[i,:],"r")
            # oplot(mz_pred,int_pred,"b-.")
            # oplot(mz_pred,int_pred,"g--")
            # xlim(343.18,343.4)
            # ylim(500,2500)
            # legend([" ","Centrodied data","Profile data", "Predicted profile Cauchy dist", "Predicted profile Gaussian dist"])
            # xlabel("m/z (Da)")
            # ylabel("Intensity")
            # savefig("predicted_prof3.png")

            mz_prof_pred[i,1:length(mz_pred)] =  mz_pred[:];
            int_prof_pred[i,1:length(mz_pred)] = int_pred[:];
        end

    end
    model = []
    GC.gc()
    return(mz_prof_pred,int_prof_pred)


end



################################################################
# Function to predict the Delta m/z


function dmz_pred(mass,rel_int,ret_fact)

    mm=pathof(SAFD)
    path2aux=joinpath(mm[1:end-7],"QTOF_model.joblib")
    println("The model is being loaded. This may take a few minutes.")
    # model = jl.load("/Users/saersamanipour/Desktop/dev/pkgs/cent2profile.jl/src/reg.joblib")
    model = jl.load(path2aux)
    model.verbose = 0;


    features = hcat(ret_fact,mass,rel_int)
    dm_pre = predict(model, features);

    return dm_pre

end



################################################################
# Function to predict the Delta m/z


function dmz_pred_batch(mz_val_cent,mz_int_cent)

    mm=pathof(SAFD)
    path2aux=joinpath(mm[1:end-7],"QTOF_model.joblib")
    println("The model is being loaded. This may take a few minutes.")
    # model = jl.load("/Users/saersamanipour/Desktop/dev/pkgs/cent2profile.jl/src/QTOF_model.joblib")
    model = jl.load(path2aux)
    model.verbose = 0;


    dm_pre = zeros(size(mz_val_cent));
    

    #println(dm_pre)

    #@showprogress 1 "The dm/z are being estimated ..." for i =1:size(mz_val_cent,1)
    #    sleep(0.5)
    for i in ProgressBar(1:size(mz_val_cent,1))
    #for i=1:size(mz_val_cent,1)
    #   println(i)
        ms = mz_val_cent[i,:];
        ms_int = mz_int_cent[i,:];
        if length(ms_int[ms_int .>0])>0
            rel_ms_int = 100 .* (ms_int ./ maximum(ms_int));
            rt_fact = 100 * i/size(mz_val_cent,1)
            features = hcat(rt_fact * ones(length(ms[ms .>0]),1),ms[ms .>0],rel_ms_int[ms .>0])
            dm_pre[i,ms .>0] = predict(model, features);


        end

    end
    model = []
    GC.gc()



    return dm_pre

end