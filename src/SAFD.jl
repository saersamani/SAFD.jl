__precompile__()
module SAFD

using LsqFit
using Dierckx
using Statistics
using DataFrames
using CSV
using DSP
using BenchmarkTools
using FileIO
using PyCall 
using ScikitLearn
using Conda

using ProgressBars
using Dates

Conda.add("joblib")

const jl = PyNULL()

function __init__()
    copy!(jl,pyimport("joblib"))

end






include("Centroid.jl")
include("Profile.jl")
include("FeatureDetection.jl")
include("FeatureAlign.jl")
include("PeakFind.jl")
include("TargetedFeature.jl")



export safd, safd_s3D, safd_s3d_cent, centroid, dmz_pred_batch, dmz_pred, feature_align, targetfeature




end # End of the module

#################################################################
#
